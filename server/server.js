var express = require('express');
var session = require('express-session');
var ejs = require('ejs');
var path = require('path');
var mongoose = require ("mongoose");
var bodyParser = require('body-parser');
mongoose.Promise = global.Promise;
var app = express();
mongoose.connect('mongodb://localhost:27017/ticket');
app.set('view engine','ejs');
app.use(express.static(path.join(__dirname, '/../view/css')));
app.use(express.static(path.join(__dirname, '/../view/js')));
app.use(express.static(path.join(__dirname, '/../view/html')));
app.use(session({secret: 'ssshhhhh'}));
app.get("/" , (req,res) => {
    res.sendFile(path.join(__dirname, "/../view/html/index.html"));
});

var user = mongoose.model('user',{
    email:{
        type:String,
        required:true,
    },
    username: {
        type:String,
        required:true,
        minlenght : '1',
    },
    password : {
        type:String,
        required:true,
        minlenght:'1'
    },
    entry:{
        type:Array
    }
});
app.use(bodyParser());
app.post('/',(req,res) => {
    var newuser = new user({
        email:req.body.email,
        username:req.body.username,
        password:req.body.password
    });
    newuser.save().then(() => {
        res.sendFile(path.join(__dirname, "/../view/html/index.html"));
    },(err) => {
        res.render("signup", {msg : "Please all the fields"});
    });
});

app.post('/login',(req,res) => {
    var username=req.body.username;
    var password=req.body.password;

    user.find({ $and: [{username : username}, {password : password}]}).then((doc) => {
    if(doc.length == 0){
        console.log("data not found");
        res.render("index", {msg : "Username and Password does not match"});
    }else{
        req.session.username = username;
        if(username == "admin" && password == "admin123"){
            res.redirect('/admin')
        }else{
            res.render("user", {data : doc[0].entry});
           
        }
       // req.session.username = username;
        //console.log(doc[0].entry)
        
    }
});
});
var sshs;
app.post('/newentry', (req,res)=>{
    sshs = req.session;
    var newEntry = {
    ref : Math.floor(Math.random()*1000),
    cat : req.body.category,
    disc : req.body.discription,
    status : "solve",
    };
    console.log(newEntry);
    user.update({username:sshs.username}, {$push:{entry : newEntry}}).then((doc) => {
       user.find({username:sshs.username}).then((doc1) => {
        res.render("user", {data : doc1[0].entry});
       });
            // res.redirect('/user');
    
    //console.log(doc);
})
},(err) => {
    console.log(err);
});

app.get('/admin/delete/:id',(req,res) => {
    var entry_ref = Number(req.params.id);
    console.log(entry_ref);
    user.update({},{$pull:{entry:{ref:entry_ref}}}).then((doc)=>{
        console.log(doc);
        res.redirect('/admin');
    })
});

app.get('/user/delete/:id',(req,res) => {
    sshs = req.session;
    var entry_ref = Number(req.params.id);
    console.log(entry_ref);
    user.update({username:sshs.username},{$pull:{entry:{ref:entry_ref}}}).then((doc)=>{
        console.log(doc);
        res.redirect('/user');
    })
});

app.get('/admin',(req,res)=>{
    user.find().then((doc) => {
        res.render("admin", {data : doc}); 
    })
})

app.get('/user',(req,res)=>{
    sshs = req.session;
    user.find({username:sshs.username}).then((doc1) => {
        // console.log(doc1)
        // console.log(doc1[0].entry);
        res.render("user", {data : doc1[0].entry});
    })
})

app.listen(3000,() =>{
    console.log("server started at port no 3000");
});